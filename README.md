# master_internship-greysided_vole 

# Cleaning and survival history project

Here is a project leaded while a Master graduate internship in spring 2022 by Maxime Marini (Alpine ecology student) and supervised by Nigel Gilles Yoccoz (Department of Biology, UiT Tromso, Norway), Anne Loison and Glenn Yannic (Laboratory of Alpine Ecology, Grenoble, France).

## Objective

The goal of this project is to build a script to clean up a data set obtained from a long term fauna's monitoring by Capture-Mark-Recapture (CMR) methods.

Then, a table is displayed where one can read a survival history for each individual that has been tracked. Captured individuals are scored "1" and uncaptured "0", per capture session. It's possible to work on 1st capture session or 2nd capture session.

Here is definitely a first version about this script project. Of course, he has to be continued and edited to be improved ...


## Notice

Run the whole "0-files-sourcing.R" located at the root of the project

Click on the "README_FR.pdf" or "README_EN.pdf" located at the root of the project could help to understand how the scripts are operating.

You can see "sample" table stored in data file data/reshaped/porsV2.RData which is an overview about which kind of data can be used into this script.

## Contributing
It's of course possible that some things were missed or wrongs. Just let me know by e-mail or as described below :

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## To do list

- Compute the p parameter IC
- Work on 2nd capture session and check the GOF
- Work with age co variate into the models
- Build a classic CJS model and compare results with those from models previously built

## Contact

marini.maxime@hotmail.fr