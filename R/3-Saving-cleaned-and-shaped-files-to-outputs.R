
## Type   :  Master's internship 
## Auteur :  Maxime Marini
## Topic  :  Study of the seasonal survival rate's evolution about grey-sided voles in Fennoscandia's populations 

## Main   :  Save the cleaned files and mains datasets, required for the 41st R-Script where are applied treatments and analysis



## - Save the 'ch_table', 'pors', 'Apors' and the 'index_table' in the output folder                                                                                            
## -----------------------------------------------------------------------------------------------------------------------------------------

 # Setting the directory
  path_data     <- paste(path_project, "output", "data", sep = "/")

 # Clean the output folder
  setwd(path_data)
  unlink(list.files())

 # Save the outputs .csv (any software can open but do not conserve variable type) and .Rdata (R software required but conserve variable type)
  write.csv(ch_table, file = "ch_table.csv")
  save(ch_table, file = "ch_table.Rdata")
  
  write.csv(Apors, file = "Apors.csv")
  save(Apors, file = "Apors.Rdata")
  
  write.csv(index_table, file = "index_table.csv")
  save(index_table, file = "index_table.Rdata")
  
  write.csv(surv_story, file = "surv_story.csv")
  save(surv_story, file = "surv_story.Rdata")
  
  write.csv(pors, file = "pors.csv")
  save(pors, file = "pors.Rdata")

 # Save the output .inp for MARK or U-Care
  write.table(ch, file = "ch.inp", sep = " ", quote = F, col.names = F, row.names = F) # without covariates
  
  
