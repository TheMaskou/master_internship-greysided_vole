
## Type   :  Master's internship 
## Auteur :  Maxime Marini
## Topic  :  Study of the seasonal survival rate's evolution about grey-sided voles in Fennoscandia's populations  

# Carried into :
# J. Laake, « RMark: an R Interface for analysis of capture–recapture data with MARK », AFSC Processed Report 2013-01. 29 octobre 2014.

## Main   :  Example : Make the .inp file for MARK and then RMark


# simple code to generate encounter history .inp files for MARK from CJS data

# clean up a few things, and set wd
rm(list=ls(all=TRUE))

# - Setting the directory
set_wd_script <- function() {
  library(rstudioapi)
  current_path <- getActiveDocumentContext()$path
  setwd(dirname(current_path))
  print(getwd())
}

# - Finding the emplacement of the project, the actual script, where to pick data sets and where to let outputs analysis
path_script   <- set_wd_script()
path_project  <- str_sub(path_script, end = -3)
path_data     <- paste(path_project, "data", sep = "/")
path_output   <- paste(path_project, "output", "data", sep = "/")

# load package "reshape" and datas
library(reshape)
setwd(path_data)

# read in the data - assumes .csv file minimially contains individual identification field
# called ’Tag’ and the encounter year as ’Year’. Following code assumes ’Tag’ is in the
# first column, and ’Year’ is in some other column (say, the second column).
data = read.csv("cjs-pivot.csv", sep = ";", dec = ".")
data = select(data, Tag, Year) #modif perso
data = data[1:24,]             #modif perso
data = data.frame(data) ; data

setwd(path_output)             #modif perso

# now, we reshape the data using melt
transform = melt(data, id.vars = "Tag")
pivot = cast(transform, Tag ~ value)
pivot[is.na(pivot)] = 0

# turns all non-zero matrix elements (for year, not tag) into 1
# following assumes Tag is in the first column of pivot
pivot[,2:ncol(pivot)][pivot[,2:ncol(pivot)] != 0] = 1

## now get everything ready to output the CH ##
# find length of history
lh <- max(data$Year)-min(data$Year)+2;

# following code needed to accommodate any missing years of data. Basic logic
# is to identify missing occasions by comparing columns in pivot table with
# a canonical list (occStr) of occasions (years) that should be in the table...
occStr <- seq(min(data$Year),max(data$Year),1); # vector of years you want
occStr <- as.character(occStr); # convert to character string
occStr <- c("Tag",occStr); # pre-pend tag onto occStr
Missing <- setdiff(occStr,names(pivot)) # Find names of missing columns
pivot[Missing] <- 0 # Add them, filled with ’0’s or dots
pivot <- pivot[occStr] # sort in ordinal sequence

#
# now do formatting of encounter history file
#

pivot$eh <- apply(pivot[2:lh],1,paste,collapse="") # concatenates encounter columns into eh
pivot[2:lh] <- NULL # drops individual encounter columns

# create commented tag
pivot$Tag <- paste("/*", pivot$Tag, "*/", sep=" ")

# sort by descending encounter histories
pivot <- pivot[order(pivot$eh,decreasing=TRUE),]

# tack on the frequency for the individual
pivot$end <- "1;";

# write out the input file
write.table(pivot,file="cjs-pivot.inp",sep=" ",quote=F,col.names=F,row.names=F)

